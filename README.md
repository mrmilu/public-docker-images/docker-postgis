# docker-postgis

Postgis multiarch images with some locales added (locales are needed for proper collation support)

Collations added are:
* es_ES.UTF8
* en_EN.UTF8

## How to build

Install qemu with arm support

```shell
$ sudo apt install qemu-system-arm
```

You need install [docker buildx](https://github.com/docker/buildx/#installing)

To push images to repo you need configure credentials.

[comment]: <> (# TODO: How to configure credentials )

Now you can build images for you local use:

```shell
$ make
```

Or run deploy to production (versions are hardcoded for now):

```shell
$ make push
```

## TO DO

See https://salsa.debian.org/debian-gis-team/postgis/-/blob/master/debian/control for debian deps
And https://postgis.net/docs/manual-3.1/postgis_installation.html#PGInstall for how to compile from sources
Then add multistage builds to do all